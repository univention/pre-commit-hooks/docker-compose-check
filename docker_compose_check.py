#!/usr/bin/env python3

import collections
import subprocess
import sys

from os.path import dirname


def main():
    directories = collections.defaultdict(list)

    for full_path in sys.argv[1:]:
        directories[dirname(full_path)].append(full_path)

    for directory, files in directories.items():
        file_arguments = [argument for file_name in files
                          for argument in ('--file', file_name)]
        command = ['docker-compose', *file_arguments, 'config', '--quiet']
        docker_compose_process = subprocess.Popen(
                command, stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT)

        stdout, stderr = docker_compose_process.communicate()
        if docker_compose_process.returncode != 0:
            print(f'Command: {" ".join(command)}')
            print(f'Output: {stdout.decode()}')
            return docker_compose_process.returncode

    return 0


if __name__ == '__main__':
    sys.exit(main())
